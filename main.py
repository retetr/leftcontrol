import pygame
import pygame.locals
import time
import sys
import math
import queue
import subprocess
import webbrowser
import multiprocessing
import threading
import os
import wmi
import datetime as dt

import keyboard

import win32api
import win32com.client as comclt
import win32clipboard

import audioController as ac
import scroller
import runners

# https://docs.microsoft.com/en-us/windows/win32/inputdev/virtual-key-codes
VK_MEDIA_NEXT_TRACK = 0xB0
VK_MEDIA_PREV_TRACK = 0xB1
VK_MEDIA_STOP = 0xB2
VK_MEDIA_PLAY_PAUSE = 0xB3
VK_LAUNCH_MEDIA_SELECT = 0xB5
VK_APPS = 0x5D

startupinfo = subprocess.STARTUPINFO()
startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
startupinfo.wShowWindow = 1

# pygame.init()
#pygame.joystick.init()

sbc = wmi.WMI(namespace='wmi').WmiMonitorBrightnessMethods()[0]


def register_browsers():
    ffpath = 'C:\\Program Files\\Mozilla Firefox\\firefox.exe'
    webbrowser.register('firefox', None, webbrowser.GenericBrowser(ffpath))
    chromepath = 'C:\\Program Files\\Google\\Chrome\\Application\\Chrome.exe'
    webbrowser.register('chrome', None, webbrowser.GenericBrowser(chromepath))


class AltHold():
    def __init__(self, key, on_press, on_hold, delay):
        self.init_time = dt.datetime.now()
        self.key = key
        self.on_press = on_press
        self.on_hold = on_hold

        self.on_hold_timer = threading.Timer(delay, self.runner)
        self.on_hold_timer.start()
        self.on_hold_ran = False


    def release(self):
        # hold_time = dt.datetime.now() - self.init_time
        # print('{} release {}'.format(self.key, hold_time.total_seconds()))
        
        if not self.on_hold_ran:
            self.on_hold_timer.cancel()
            self.on_press()
        
    def runner(self):
        # hold_time = dt.datetime.now() - self.init_time
        self.on_hold_ran = True
        self.on_hold_timer.cancel()
        self.on_hold()

class Main(object):
    def __init__(self, args):

        pygame.init()
        pygame.joystick.init()

        self.axisHandlers = {
            2: self._f_axis,
            5: self._g_axis,
            7: self._rty_4_axis,
        }
        self.buttonDownHandlers = {
            0: self._e,
            1: self._f_bump,
            2: self._g_bump,
            3: self._i,
            4: self._h,
            5: self._sw_1,
            6: self._sw_2,
            7: self._sw_3,
            8: self._sw_4,
            9: self._sw_5,
            10: self._sw_6,
            27: self._k1_back,
            28: self._k1_fwd,
            33: self._m1,
            34: self._m2,
            35: self._s1
        }
        self.buttonUpHandlers = {}

        self.running_handlers = {}
    
        self.running = True

        self.scroll_speed = 0

        self.brightness = 0

    def run(self):
        joystick_count = pygame.joystick.get_count()
        joystick = pygame.joystick.Joystick(0)
        joystick.init()

        print("Number of joysticks: {}".format(joystick_count))
        clock = pygame.time.Clock()
        while self.running:
            try:
                print('awaiting event')
                event = pygame.event.wait()
            except KeyError:
                print("Succesfully caught a key error")
                
                
            print('event triggered')
            if event.type == pygame.JOYAXISMOTION:
                try:
                    self.axisHandlers[event.axis](event.axis, event.value)
                except KeyError:
                    print("Are you missing an AXIS handler for {}?".format(event.axis))

            elif event.type == pygame.JOYBUTTONDOWN:
                key = str(event.button)
                try:
                    self.buttonDownHandlers[event.button](key)
                except KeyError:
                    print("Are you missing a BUTTON handler for {}?".format(event.button))
            elif event.type == pygame.JOYBUTTONUP:
                # try:
                #     self.buttonUpHandlers[event.button]()
                # except KeyError:
                key = str(event.button)
                if key in self.running_handlers:
                    self.running_handlers[key].release()
                else:
                    print("Are you missing a BUTTON UP handler for {}?".format(event.button))

            clock.tick(60)

    # Mode Switches
    def _m1(self, key):
        # Same but in Chrome?
        pass

    def _m2(self, key):
        # This could be our browser favorites?
        pass

    def _s1(self, key):
        # Maybe our regular mode?
        pass

    def _g_axis(self, key, value):
        norm = self._normalize(value)
        print("Z Axis: {} {}".format(value, norm))
        ac.set_volume(norm)

    def _g_bump(self, key):
        ac.toggle_mute()

    def _normalize(self, value):
        temp = value + 1
        if temp > 0:
            return temp/2
        else:
            return 0

    def _sw_1(self, key):
        # open Firefox
        #subprocess.Popen(['C:\Program Files\Mozilla Firefox\\firefox.exe'])
        webbrowser.get('chrome').open_new('https://www.reddit.com/r/IASIP/')

    def _sw_2(self, key):
        # open Firefox
        #subprocess.Popen(['C:\Program Files\Mozilla Firefox\\firefox.exe', '-new-tab'])
        webbrowser.get('firefox').open_new_tab('https://stackoverflow.com/questions/tagged/javascript?tab=Frequent')

    def _sw_3(self, key):
        # Open KeePass
        print('Opening KeePass')
        pathsToKeePass = ["C:\\Program Files (x86)\\KeePass Password Safe 2\\KeePass.exe", "C:\\Users\\peter\\PortableApps\\KeePass\\KeePass.exe"]
        for path in pathsToKeePass:
            print('Looking in {}'.format(path))
            if os.path.exists(path):
                subprocess.Popen(path, startupinfo=startupinfo)
                break


    def _sw_4(self, key):
        # open chrome new tab
        subprocess.Popen('explorer "C:\\Users\\peter"', startupinfo=startupinfo)

    def _sw_5(self, key):
        # Open CGFSWIN10DEV RDP
        path = "C:\\Users\\peter\\RemoteMachines\\CGFSWIN10DEV1.rdp"
        win10dev = runners.rdp(path)
        
        self.running_handlers[key] = AltHold(
            key, win10dev.open, win10dev.edit, 1.5
            )
        # for path in pathsToDevRDP:
        #     print('Looking in {}'.format(path))
        #     if os.path.exists(path):
        #         subprocess.call(["C:\\Windows\\system32\\mstsc.exe", path])
        #         break
            


    def _sw_6(self, key):
        # Open Base2 RDP
        pathsToDevRDP = ["C:\\Users\\peter\\RemoteMachines\\base2.rdp"]
        path = "C:\\Users\\peter\\RemoteMachines\\base2.rdp"
        base2 = runners.rdp(path)

        self.running_handlers[key] = AltHold(
            key, base2.open, base2.edit, 1.5
            )
        # for path in pathsToDevRDP:
        #     print('Looking in {}'.format(path))
        #     if os.path.exists(path):
        #         subprocess.call(["C:\\Windows\\system32\\mstsc.exe", path])
        #         break

    def _rty_4_axis(self, key, value):
        norm = self._normalize(value)
        # Let's use this one turn the screen brightness
        new_brightness = math.floor(norm * 100)
        if (new_brightness != self.brightness):
            self.brightness = new_brightness
            print('Setting Brightness {}'.format(self.brightness))
            sbc.WmiSetBrightness(self.brightness, 0)

    def _e(self, key):
        # get clipboard data
        win32clipboard.OpenClipboard()
        data = win32clipboard.GetClipboardData()
        win32clipboard.CloseClipboard()

        #wsh = comclt.Dispatch("WScript.Shell")
        for key in data:
            keyboard.write(key, delay=.02)


    def _h(self, key):
        win32api.keybd_event(VK_MEDIA_PLAY_PAUSE, 0)

    def _i(self, key):
        win32api.keybd_event(VK_MEDIA_STOP, 0)

    def _k1_back(self, key):
        win32api.keybd_event(VK_MEDIA_PREV_TRACK, 0)

    def _k1_fwd(self, key):
        win32api.keybd_event(VK_MEDIA_NEXT_TRACK, 0)

    def _f_bump(self, key):
        print('Going home')
        wsh = comclt.Dispatch("WScript.Shell")
        wsh.SendKeys('^{HOME}')

    def _f_axis(self, key, value):
        rounded = round(value, 1)
        if self.scroll_speed != rounded:
            self.scroll_speed = rounded
            print('speed {}'.format(rounded))
            #stop = math.isclose(0, value, abs_tol=.1)

            if hasattr(self, 'scroller') and self.scroller.is_alive():
                self.parent_conn.send(rounded)

            elif rounded:
                self.parent_conn, child_conn = multiprocessing.Pipe()
                #print('Starting thread')
                self.scroller = scroller.ProcessScroll(rounded, child_conn)
                self.scroller.start()
                #self._scroll_start(value)


if __name__ == "__main__":
    register_browsers()
    m = Main(sys.argv)
    running = True
    while running:
        try:
            m.run()
        except KeyboardInterrupt:
            print('Caught keyboard interrupt, press ctrl+c to quit')
            time.sleep(2)
            running = False