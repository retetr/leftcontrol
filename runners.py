import subprocess
import os

startupinfo = subprocess.STARTUPINFO()
startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
startupinfo.wShowWindow = 1

class rdp():
    def __init__(self, path):
        self.path = path
    
    def open(self):
        if os.path.exists(self.path):
                subprocess.Popen(["C:\\Windows\\system32\\mstsc.exe", self.path], startupinfo=startupinfo)

    def edit(self):
        if os.path.exists(self.path):
                subprocess.Popen(["C:\\Windows\\system32\\mstsc.exe", "/edit", self.path])
