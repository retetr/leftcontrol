import pygame
import sched, time
import threading
import subprocess
import os
import runners

import datetime as dt
path = "C:\\Users\\peter\\RemoteMachines\\base2.rdp"
def open_rdp():
    subprocess.call(["C:\\Windows\\system32\\mstsc.exe", path])

def edit_rdp():
    subprocess.run(["C:\\Windows\\system32\\mstsc.exe", "/edit", path])


class AltHold():
    def __init__(self, key, on_press, on_hold, delay):
        self.init_time = dt.datetime.now()
        self.key = key
        self.on_press = on_press
        self.on_hold = on_hold

        self.on_hold_timer = threading.Timer(delay, self.runner)
        self.on_hold_timer.start()
        self.on_hold_ran = False


    def release(self):
        # hold_time = dt.datetime.now() - self.init_time
        # print('{} release {}'.format(self.key, hold_time.total_seconds()))
        
        if not self.on_hold_ran:
            self.on_hold_timer.cancel()
            self.on_press()
        
    def runner(self):
        # hold_time = dt.datetime.now() - self.init_time
        self.on_hold_ran = True
        self.on_hold_timer.cancel()
        self.on_hold()
        print('{} Held'.format(self.key))


class Main():
    def __init__(self):
        print("initing")
        pygame.init()

        size = [500, 700]
        screen = pygame.display.set_mode(size)

        pygame.display.set_caption("My Game")

        self.running_handlers = {}
    

    def run(self):
        print('Running')
        clock = pygame.time.Clock()
        while True:
            event = pygame.event.wait()
            #print('New event ')
            #print(event)
            if event.type == pygame.QUIT:
                break
            
            if event.type == pygame.KEYDOWN:
                key = str(event.key)
                print('{} down'.format(key))
                if event.unicode == 'r':
                    self._r(key)

            elif event.type == pygame.KEYUP:
                key = str(event.key)
                print('{} up'.format(key))
                if key in self.running_handlers:
                    print(self.running_handlers)
                    self.running_handlers[key].release()


            clock.tick(120)

    def _r(self, key):
        path = "C:\\Users\\peter\\RemoteMachines\\CGFSWIN10DEV1.rdp"
        base2 = runners.rdp(path)
        
        self.running_handlers[key] = AltHold(
            key, base2.open, base2.edit, 1.5
            )

    def press(self):
        print('press')
        open_rdp()

    def hold(self):
        print('hold')
        edit_rdp()

if __name__ == '__main__':
    m = Main()
    m.run()