import multiprocessing
import math
import time
import win32api
import win32con


def scroll(clicks=0, delta_x=0, delta_y=0, click_mult=0):
    """
    Source: https://docs.microsoft.com/en-gb/windows/win32/api/winuser/nf-winuser-mouse_event?redirectedfrom=MSDN

    void mouse_event(
      DWORD     dwFlags,
      DWORD     dx,
      DWORD     dy,
      DWORD     dwData,
      ULONG_PTR dwExtraInfo
    );

    If dwFlags contains MOUSEEVENTF_WHEEL,
    then dwData specifies the amount of wheel movement.
    A positive value indicates that the wheel was rotated forward, away from the user;
    A negative value indicates that the wheel was rotated backward, toward the user.
    One wheel click is defined as WHEEL_DELTA, which is 120.

    :param delay_between_ticks:
    :param delta_y:
    :param delta_x:
    :param clicks:
    :return:
    """

    if clicks > 0:
        increment = win32con.WHEEL_DELTA
    else:
        increment = win32con.WHEEL_DELTA * -1

    increment = math.floor(increment*click_mult)

    for _ in range(abs(clicks)):
        win32api.mouse_event(win32con.MOUSEEVENTF_WHEEL, delta_x, delta_y, increment, 0)

class ProcessScroll(multiprocessing.Process):
    def __init__(self, value, conn):
        super(ProcessScroll, self).__init__()
        self.conn = conn
        self._set_vals(value)

    def _set_vals(self, new_velocity):
        self.direction = math.ceil(-math.copysign(1, new_velocity))
        self.interval = abs(new_velocity)

    def _scroll(self):
        stop = False
        print('_scroll entered')
        while not stop:
            if self.conn.poll():
                new_velocity = self.conn.recv()
                print('New Speed {}'.format(new_velocity))
                if new_velocity == 0:
                    stop = True
                else:
                    self._set_vals(new_velocity)
            elif self.interval:
                scroll(self.direction, 0, 0, self.interval)
                print("Still running! direction: {} interval: {}".format(self.direction, self.interval))
                time.sleep(.1)

        print('_scroll main loop end')

        new_velocity = 0
        while new_velocity == 0:
            print('_scroll awaiting new velocity {}'.format(new_velocity))
            new_velocity = self.conn.recv()

        print('_scroll new velocity {}'.format(new_velocity))
        self._set_vals(new_velocity)
        #self._scroll()

    def run(self):
        print('Starting inner')
        running = True
        while running:
            try:
                self._scroll()
            except KeyboardInterrupt:
                running = False

        print("Scroller stopped!")
